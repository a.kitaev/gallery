package ru.itmo.gallery.domain.model

import com.google.gson.annotations.SerializedName

data class Photo(
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("sizes")
    val sizes: List<PhotoSize> = emptyList(),
    @SerializedName("date")
    val date: Long = 0L
)
