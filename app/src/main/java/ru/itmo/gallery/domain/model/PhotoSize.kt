package ru.itmo.gallery.domain.model

import com.google.gson.annotations.SerializedName

data class PhotoSize(
    @SerializedName("type")
    val type: String = "",
    @SerializedName("url")
    val url: String = ""
)
