package ru.itmo.gallery.domain.repository

import ru.itmo.gallery.domain.model.Photo

interface VkPhotosRepository {

    suspend fun loadPhotos(
        ownerId: Int,
        isCommunity: Boolean,
        albumId: Int,
        photoCount: Int
    ): List<Photo>
}
