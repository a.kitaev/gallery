package ru.itmo.gallery.domain.repository

import androidx.fragment.app.FragmentActivity
import com.vk.api.sdk.auth.VKAccessToken

interface VkAuthRepository {

    fun isUserAuthorize(): Boolean

    fun login(activity: FragmentActivity)

    fun saveToken(accessToken: VKAccessToken)

    fun removeToken()
}
