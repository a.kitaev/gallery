package ru.itmo.gallery.domain.model

data class UrlWithDate(
    val url: String,
    val date: Long
)
