package ru.itmo.gallery.domain.interactor

import androidx.fragment.app.FragmentActivity
import com.vk.api.sdk.auth.VKAccessToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.itmo.gallery.domain.model.Photo
import ru.itmo.gallery.domain.model.PhotoSize
import ru.itmo.gallery.domain.model.UrlWithDate
import ru.itmo.gallery.domain.repository.VkAuthRepository
import ru.itmo.gallery.domain.repository.VkPhotosRepository

class VkApiInteractor(
    private val vkAuthRepository: VkAuthRepository,
    private val vkPhotosInteractor: VkPhotosRepository
) {

    companion object {

        private const val OWNER_ID: Int = 128666765
        private const val ALBUM_ID: Int = 266276915
        private const val MAX_PHOTO_COUNT: Int = 1000
    }

    fun isUserAuthorize(): Boolean =
        vkAuthRepository.isUserAuthorize()

    fun login(activity: FragmentActivity) {
        vkAuthRepository.login(activity)
    }

    fun saveToken(accessToken: VKAccessToken) {
        vkAuthRepository.saveToken(accessToken)
    }

    fun removeToken() {
        vkAuthRepository.removeToken()
    }

    @Throws(Exception::class)
    suspend fun getLargePhotoUrlsWithDate(): List<UrlWithDate> =
        getPhotoUrlsWithDateBySize { photoSizes ->
            photoSizes.maxByOrNull { it.type }
        }

    @Throws(Exception::class)
    suspend fun getSmallPhotoUrlsWithDate(): List<UrlWithDate> =
        getPhotoUrlsWithDateBySize { photoSizes ->
            photoSizes.minByOrNull { it.type }
        }

    @Throws(Exception::class)
    private suspend fun getPhotoUrlsWithDateBySize(
        selector: (List<PhotoSize>) -> PhotoSize?
    ): List<UrlWithDate> {
        val photos = getPhotos()
        val urlsWithDate = mutableListOf<UrlWithDate>()
        photos.forEach { photo ->
            val photoSize = selector.invoke(photo.sizes)
            photoSize?.let { urlsWithDate.add(UrlWithDate(it.url, photo.date)) }
        }
        return urlsWithDate
    }

    @Throws(Exception::class)
    private suspend fun getPhotos(): List<Photo> = withContext(Dispatchers.IO) {
        vkPhotosInteractor.loadPhotos(
            ownerId = OWNER_ID,
            isCommunity = true,
            albumId = ALBUM_ID,
            photoCount = MAX_PHOTO_COUNT
        )
    }
}
