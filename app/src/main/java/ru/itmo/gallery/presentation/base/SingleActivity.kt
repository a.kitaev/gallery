package ru.itmo.gallery.presentation.base

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAuthCallback
import ru.itmo.gallery.R

class SingleActivity : AppCompatActivity(), BackPressedCallbackResolver {

    private val callbackList = mutableListOf<Pair<Int, () -> Any>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    @Synchronized
    override fun addBackCallback(key: Int, callback: () -> Any) {
        callbackList.add(key to callback)
    }

    @Synchronized
    override fun removeBackCallback(key: Int) {
        callbackList.removeAll { it.first == key }
    }

    @Synchronized
    override fun callParentCallback(key: Int) {
        for (i in 0 until callbackList.size) {
            if (callbackList[i].first == key) {
                when (i) {
                    0 -> super.onBackPressed()
                    else -> callbackList[i - 1].second.invoke()
                }
            }
        }
    }

    @Synchronized
    override fun callChildCallback(key: Int) {
        if (callbackList.isNotEmpty() && callbackList.last().first != key) {
            callbackList.last().second.invoke()
        }
    }

    override fun onBackPressed() {
        if (callbackList.isEmpty()) {
            super.onBackPressed()
        } else {
            callbackList.last().second.invoke()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // https://github.com/VKCOM/vk-android-sdk/issues/421
        supportFragmentManager.fragments.forEach { fragment ->
            fragment.childFragmentManager.fragments.forEach { childFragment ->
                if (childFragment is VKAuthCallback) {
                    VK.onActivityResult(requestCode, resultCode, data, childFragment)
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
