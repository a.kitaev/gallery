package ru.itmo.gallery.presentation.ui.album

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.squareup.picasso.Picasso
import ru.itmo.gallery.domain.model.UrlWithDate
import ru.itmo.gallery.presentation.utils.throttleClick
import ru.itmo.gallery.presentation.view.GridItemView

class ImageAdapter(
    private val context: Context,
    private val onClickListener: (GridItemView, Int) -> Unit
) : BaseAdapter() {

    var urlsWithDate: Array<UrlWithDate> = emptyArray()
        private set

    fun update(photoUrls: List<UrlWithDate>) {
        urlsWithDate = photoUrls.toTypedArray()
        notifyDataSetChanged()
    }

    override fun getCount(): Int = urlsWithDate.size

    override fun getItem(position: Int): UrlWithDate = urlsWithDate[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val imageView = if (convertView == null) {
            GridItemView(context)
        } else {
            convertView as GridItemView
        }

        Picasso.get()
            .load(urlsWithDate[position].url)
            .into(imageView)

        imageView.scaleType = ImageView.ScaleType.CENTER_CROP
        imageView.throttleClick {
            onClickListener.invoke(imageView, position)
        }

        return imageView
    }
}
