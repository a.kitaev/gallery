package ru.itmo.gallery.presentation.view

import android.content.Context
import android.net.Uri
import android.util.AttributeSet
import android.widget.FrameLayout
import com.stfalcon.imageviewer.listeners.OnImageChangeListener
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import ru.itmo.gallery.R
import ru.itmo.gallery.databinding.ViewPhotoOverlayBinding
import ru.itmo.gallery.domain.model.UrlWithDate
import ru.itmo.gallery.presentation.utils.ImageLoaderUtil
import ru.itmo.gallery.presentation.utils.throttleClick

class PhotoOverlayView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle), OnImageChangeListener {

    companion object {

        private const val DATE_PATTERN = "d MMMM yyyy"
        private const val MILLISECOND_IN_SECOND = 1000L
    }

    private val binding: ViewPhotoOverlayBinding
    private var dataLoader: ((Int) -> UrlWithDate)? = null

    private var currentUrlWithDate: UrlWithDate? = null
        set(value) {
            field = value
            updateTitle()
        }

    init {
        val root = inflate(context, R.layout.view_photo_overlay, this)
        binding = ViewPhotoOverlayBinding.bind(root)
    }

    fun setDataLoader(dataLoader: (Int) -> UrlWithDate) {
        this.dataLoader = dataLoader
    }

    fun setOnBackClickListener(callback: () -> Unit) {
        binding.toolbar.setNavigationOnClickListener {
            callback.invoke()
        }
    }

    fun setOnShareClickListener(
        onSuccess: (Uri) -> Unit,
        onFailure: (Throwable) -> Unit
    ) {
        binding.shareButton.throttleClick {
            ImageLoaderUtil.loadImageUri(
                url = currentUrlWithDate?.url.orEmpty(),
                onSuccess = onSuccess,
                onFailure = onFailure
            )
        }
    }

    override fun onImageChange(position: Int) {
        this.currentUrlWithDate = dataLoader?.invoke(position)
    }

    private fun updateTitle() {
        val timestamp = currentUrlWithDate?.date
        binding.toolbar.title = getFormatDate(timestamp)
    }

    private fun getFormatDate(timestamp: Long?): String? =
        try {
            timestamp?.let {
                SimpleDateFormat(DATE_PATTERN, Locale.getDefault())
                    .format(Date(it * MILLISECOND_IN_SECOND))
            }
        } catch (e: Exception) {
            null
        }
}
