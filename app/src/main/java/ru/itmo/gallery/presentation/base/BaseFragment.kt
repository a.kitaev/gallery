package ru.itmo.gallery.presentation.base

import androidx.fragment.app.Fragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import ru.itmo.gallery.R

abstract class BaseFragment : Fragment() {

    protected fun showError(throwable: Throwable) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.error)
            .setMessage(throwable.localizedMessage ?: getString(R.string.error_default_description))
            .setCancelable(true)
            .setPositiveButton(R.string.ok) { dialogInterface, _ ->
                dialogInterface.dismiss()
            }
            .show()
    }
}
