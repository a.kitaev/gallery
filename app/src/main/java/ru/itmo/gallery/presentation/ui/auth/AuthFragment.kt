package ru.itmo.gallery.presentation.ui.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKAuthCallback
import com.vk.api.sdk.exceptions.VKAuthException
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.itmo.gallery.databinding.FragmentAuthBinding
import ru.itmo.gallery.presentation.base.NavigationFragment
import ru.itmo.gallery.presentation.utils.observe
import ru.itmo.gallery.presentation.utils.throttleClick

class AuthFragment : NavigationFragment(), VKAuthCallback {

    private val viewModel by viewModel<AuthViewModel>()
    private lateinit var binding: FragmentAuthBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAuthBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        observe(viewModel.state, this::render)
    }

    private fun initViews() {
        binding.enterVkButton.throttleClick {
            viewModel.onVkClicked(requireActivity())
        }
    }

    private fun render(viewState: AuthViewState) {
        viewState.event?.let { handleEvent(it) }
    }

    private fun handleEvent(event: AuthViewState.Event) {
        when (event) {
            is AuthViewState.Event.Error -> showError(event.throwable)
            is AuthViewState.Event.Navigation -> navController.navigate(event.destination)
        }
        viewModel.onEventHandled()
    }

    override fun onLogin(token: VKAccessToken) {
        viewModel.onLoginSuccess(token)
    }

    override fun onLoginFailed(authException: VKAuthException) {
        viewModel.onLoginFailed(authException)
    }

    override fun onBackPressed() {
        callParentBackPressedCallback()
    }
}
