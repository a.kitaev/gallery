package ru.itmo.gallery.presentation.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Environment
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import ru.itmo.gallery.presentation.base.App

object ImageLoaderUtil {

    fun loadImageUri(
        url: String,
        onSuccess: (Uri) -> Unit,
        onFailure: (Throwable) -> Unit
    ) {
        try {
            Picasso.get()
                .load(url)
                .into(BaseTarget(uriListener = onSuccess))
        } catch (e: Throwable) {
            onFailure.invoke(e)
        }
    }

    private fun getLocalBitmapUri(context: Context, bmp: Bitmap): Uri {
        var bmpUri: Uri? = null
        try {
            val fileName = "share_image_" + System.currentTimeMillis() + ".png"
            val file = File(
                context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                fileName
            )
            val out = FileOutputStream(file)
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out)
            out.close()
            bmpUri = Uri.fromFile(file)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bmpUri ?: throw IllegalStateException("Ошибка сохранения фото")
    }

    private open class BaseTarget(
        private val bitmapListener: ((Bitmap) -> Unit)? = null,
        private val uriListener: ((Uri) -> Unit)? = null
    ) : Target {

        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            bitmap?.let { bmp ->
                bitmapListener?.invoke(bmp)
                uriListener?.invoke(getLocalBitmapUri(App.appContext, bmp))
            }
        }

        override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {
            if (e != null) {
                throw e
            }
        }

        override fun onPrepareLoad(placeHolderDrawable: Drawable?) = Unit
    }
}
