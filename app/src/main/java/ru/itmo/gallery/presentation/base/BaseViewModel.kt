package ru.itmo.gallery.presentation.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.itmo.gallery.presentation.utils.onNext

abstract class BaseViewModel<T>(initValue: T) : ViewModel() {

    protected val _state = MutableLiveData<T>()
    val state: LiveData<T> = _state

    protected var viewState: T = initValue
        set(value) {
            field = value
            _state.onNext(value)
        }

    abstract fun onEventHandled()
}
