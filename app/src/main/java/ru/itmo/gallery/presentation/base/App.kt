package ru.itmo.gallery.presentation.base

import android.app.Application
import android.content.Context
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import ru.itmo.gallery.BuildConfig
import ru.itmo.gallery.presentation.di.contextModule
import ru.itmo.gallery.presentation.di.interactorModule
import ru.itmo.gallery.presentation.di.repositoryModule
import ru.itmo.gallery.presentation.di.storageModule
import ru.itmo.gallery.presentation.di.viewModelModule
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        appInstance = this
        startKoin()
        startLogger()
        val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
    }

    private fun startKoin() {
        startKoin {
            androidContext(this@App)
            modules(
                listOf(
                    contextModule,
                    viewModelModule,
                    interactorModule,
                    repositoryModule,
                    storageModule
                )
            )
        }
    }

    private fun startLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    companion object {

        lateinit var appInstance: App
            private set

        val appContext: Context
            get() = appInstance.applicationContext
    }
}
