package ru.itmo.gallery.presentation.ui.auth

import androidx.fragment.app.FragmentActivity
import com.vk.api.sdk.auth.VKAccessToken
import ru.itmo.gallery.domain.interactor.VkApiInteractor
import ru.itmo.gallery.presentation.base.BaseViewModel

class AuthViewModel(
    private val vkApiInteractor: VkApiInteractor
) : BaseViewModel<AuthViewState>(
    initValue = AuthViewState()
) {

    init {
        if (vkApiInteractor.isUserAuthorize()) {
            navigateToAlbum()
        }
    }

    override fun onEventHandled() {
        viewState = viewState.copy(event = null)
    }

    fun onVkClicked(activity: FragmentActivity) {
        vkApiInteractor.login(activity)
    }

    fun onLoginSuccess(accessToken: VKAccessToken) {
        vkApiInteractor.saveToken(accessToken)
        navigateToAlbum()
    }

    fun onLoginFailed(throwable: Throwable) {
        // ignore
    }

    private fun navigateToAlbum() {
        viewState = viewState.copy(
            event = AuthViewState.Event.Navigation(
                AuthFragmentDirections.toAlbum()
            )
        )
    }
}
