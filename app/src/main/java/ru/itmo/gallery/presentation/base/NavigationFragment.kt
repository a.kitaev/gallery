package ru.itmo.gallery.presentation.base

import androidx.navigation.fragment.findNavController
import kotlin.random.Random

abstract class NavigationFragment : BaseFragment() {

    private val uid = Random.nextInt()

    protected val navController by lazy {
        findNavController()
    }

    private val callbackResolver by lazy {
        requireActivity() as BackPressedCallbackResolver
    }

    override fun onResume() {
        super.onResume()
        callbackResolver.addBackCallback(uid) {
            onBackPressed()
        }
    }

    override fun onPause() {
        callbackResolver.removeBackCallback(uid)
        super.onPause()
    }

    protected open fun onBackPressed() {
        navController.navigateUp()
    }

    protected fun callParentBackPressedCallback() {
        callbackResolver.callParentCallback(uid)
    }

    protected fun callChildBackPressedCallback() {
        callbackResolver.callChildCallback(uid)
    }
}
