package ru.itmo.gallery.presentation.utils

import android.content.Intent
import android.net.Uri
import androidx.fragment.app.Fragment
import ru.itmo.gallery.R

object ShareUtil {

    private const val PHOTO_TYPE = "image/jpeg"

    fun sharePhoto(fragment: Fragment, uri: Uri) {
        val shareIntent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_STREAM, uri)
            type = PHOTO_TYPE
        }
        fragment.startActivity(
            Intent.createChooser(
                shareIntent,
                fragment.getString(R.string.share_description)
            )
        )
    }
}
