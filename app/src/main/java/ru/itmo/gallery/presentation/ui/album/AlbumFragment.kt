package ru.itmo.gallery.presentation.ui.album

import android.net.Uri
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.stfalcon.imageviewer.StfalconImageViewer
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.itmo.gallery.R
import ru.itmo.gallery.databinding.FragmentAlbumBinding
import ru.itmo.gallery.domain.model.UrlWithDate
import ru.itmo.gallery.presentation.base.NavigationFragment
import ru.itmo.gallery.presentation.utils.ShareUtil
import ru.itmo.gallery.presentation.utils.observe
import ru.itmo.gallery.presentation.utils.throttleClick
import ru.itmo.gallery.presentation.view.GridItemView
import ru.itmo.gallery.presentation.view.PhotoOverlayView

class AlbumFragment : NavigationFragment() {

    private val viewModel by viewModel<AlbumViewModel>()
    private lateinit var binding: FragmentAlbumBinding

    private val gridAdapter by lazy(LazyThreadSafetyMode.NONE) {
        ImageAdapter(requireContext(), this::onImageClick)
    }

    private var photoView: StfalconImageViewer<UrlWithDate>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAlbumBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        observe(viewModel.state, this::render)
    }

    private fun initViews() {
        binding.exitButton.throttleClick {
            viewModel.onExitClicked()
        }

        binding.gridView.adapter = gridAdapter
    }

    private fun render(viewState: AlbumViewState) {
        viewState.event?.let { handleEvent(it) }

        when (val screenState = viewState.screenState) {
            is AlbumViewState.ScreenState.Content -> gridAdapter.update(screenState.urlsWithDate)
            is AlbumViewState.ScreenState.Loading -> {
            }
        }
    }

    private fun handleEvent(event: AlbumViewState.Event) {
        when (event) {
            is AlbumViewState.Event.Error -> showError(event.throwable)
            is AlbumViewState.Event.Navigation -> navController.navigate(event.destination)
        }
        viewModel.onEventHandled()
    }

    private fun onImageClick(targetImageView: GridItemView, position: Int) {
        val overlayView = PhotoOverlayView(requireContext()).apply {
            setOnBackClickListener { photoView?.close() }
            setOnShareClickListener(this@AlbumFragment::onSharePhoto, this@AlbumFragment::showError)
            setDataLoader { gridAdapter.getItem(it) }
            onImageChange(position)
        }

        val typedValue = TypedValue()
        val theme = requireContext().theme
        theme.resolveAttribute(R.attr.colorPrimary, typedValue, true)
        val backgroundColor = typedValue.data

        photoView = StfalconImageViewer.Builder(context, gridAdapter.urlsWithDate, this::loadImage)
            .withStartPosition(position)
            .withHiddenStatusBar(false)
            .withBackgroundColor(backgroundColor)
            .withOverlayView(overlayView)
            .allowZooming(true)
            .withImageChangeListener(overlayView)
            .allowSwipeToDismiss(true)
            .withTransitionFrom(targetImageView)
            .show(true)
    }

    private fun loadImage(imageView: ImageView, urlWithDate: UrlWithDate) {
        Picasso.get()
            .load(urlWithDate.url)
            .into(imageView)
    }

    private fun onSharePhoto(uri: Uri) {
        ShareUtil.sharePhoto(this, uri)
    }

    override fun onBackPressed() {
        callParentBackPressedCallback()
    }
}
