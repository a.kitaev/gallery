package ru.itmo.gallery.presentation.ui.auth

import androidx.navigation.NavDirections

data class AuthViewState(
    val screenState: ScreenState = ScreenState.DefaultState,
    val event: Event? = null
) {

    sealed class ScreenState {

        object DefaultState : ScreenState()
    }

    sealed class Event {

        data class Error(val throwable: Throwable) : Event()

        data class Navigation(val destination: NavDirections) : Event()
    }
}
