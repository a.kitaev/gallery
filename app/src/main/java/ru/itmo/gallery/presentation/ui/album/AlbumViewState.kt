package ru.itmo.gallery.presentation.ui.album

import androidx.navigation.NavDirections
import ru.itmo.gallery.domain.model.UrlWithDate

data class AlbumViewState(
    val screenState: ScreenState = ScreenState.Loading,
    val event: Event? = null
) {

    sealed class ScreenState {

        object Loading : ScreenState()

        data class Content(val urlsWithDate: List<UrlWithDate>) : ScreenState()
    }

    sealed class Event {

        data class Error(val throwable: Throwable) : Event()

        data class Navigation(val destination: NavDirections) : Event()
    }
}
