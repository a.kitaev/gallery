package ru.itmo.gallery.presentation.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import ru.itmo.gallery.data.repository.VkAuthRepositoryImpl
import ru.itmo.gallery.data.repository.VkPhotosRepositoryImpl
import ru.itmo.gallery.data.storage.PersistenceStorage
import ru.itmo.gallery.domain.interactor.VkApiInteractor
import ru.itmo.gallery.domain.repository.VkAuthRepository
import ru.itmo.gallery.domain.repository.VkPhotosRepository
import ru.itmo.gallery.presentation.base.App
import ru.itmo.gallery.presentation.ui.album.AlbumViewModel
import ru.itmo.gallery.presentation.ui.auth.AuthViewModel

val contextModule = module(override = true) {

    single { App.appInstance.applicationContext }
}

val viewModelModule = module(override = true) {

    viewModel { AuthViewModel(get()) }
    viewModel { AlbumViewModel(get()) }
}

val interactorModule = module(override = true) {

    single { VkApiInteractor(get(), get()) }
}

val repositoryModule = module(override = true) {

    single { VkAuthRepositoryImpl(get()) as VkAuthRepository }
    single { VkPhotosRepositoryImpl(get()) as VkPhotosRepository }
}

val storageModule = module(override = true) {

    single { PersistenceStorage(get()) }
}
