package ru.itmo.gallery.presentation.base

interface BackPressedCallbackResolver {

    fun addBackCallback(key: Int, callback: () -> Any)

    fun removeBackCallback(key: Int)

    fun callParentCallback(key: Int)

    fun callChildCallback(key: Int)
}
