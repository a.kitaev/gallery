package ru.itmo.gallery.presentation.ui.album

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.itmo.gallery.domain.interactor.VkApiInteractor
import ru.itmo.gallery.presentation.base.BaseViewModel

class AlbumViewModel(
    private val vkApiInteractor: VkApiInteractor
) : BaseViewModel<AlbumViewState>(
    initValue = AlbumViewState()
) {

    init {
        viewModelScope.launch {
            loadPhotos()
        }
    }

    override fun onEventHandled() {
        viewState = viewState.copy(event = null)
    }

    fun onExitClicked() {
        vkApiInteractor.removeToken()
        viewState = viewState.copy(
            event = AlbumViewState.Event.Navigation(
                AlbumFragmentDirections.exitToAuth()
            )
        )
    }

    private suspend fun loadPhotos() {
        viewState = try {
            val photoUrlsWithDate = vkApiInteractor.getLargePhotoUrlsWithDate()
            viewState.copy(
                screenState = AlbumViewState.ScreenState.Content(photoUrlsWithDate)
            )
        } catch (e: Exception) {
            viewState.copy(
                screenState = AlbumViewState.ScreenState.Content(emptyList()),
                event = AlbumViewState.Event.Error(e)
            )
        }
    }
}
