package ru.itmo.gallery.data.repository

import com.vk.api.sdk.VKMethodCall
import com.vk.api.sdk.auth.VKAccessToken
import ru.itmo.gallery.data.network.response.VkPhotoResponse
import ru.itmo.gallery.data.storage.PersistenceStorage
import ru.itmo.gallery.domain.model.Photo
import ru.itmo.gallery.domain.repository.VkPhotosRepository

class VkPhotosRepositoryImpl(
    private val persistenceStorage: PersistenceStorage
) : BaseVkApiRepository(), VkPhotosRepository {

    companion object {

        private const val GET_PHOTOS_FROM_ALBUM_METHOD = "photos.get"

        private const val USER_ID_KEY = "user_id"
        private const val OWNER_ID_KEY = "owner_id"
        private const val ALBUM_ID_KEY = "album_id"
        private const val COUNT_KEY = "count"
    }

    @Throws(Exception::class)
    override suspend fun loadPhotos(
        ownerId: Int,
        isCommunity: Boolean,
        albumId: Int,
        photoCount: Int
    ): List<Photo> {
        val ownerEndId = if (isCommunity) -ownerId else ownerId
        val accessToken = VKAccessToken.restore(persistenceStorage.accessTokenStorage)
        return makeResponse(
            VKMethodCall.Builder()
                .method(GET_PHOTOS_FROM_ALBUM_METHOD)
                .args(USER_ID_KEY, accessToken?.userId ?: 0)
                .args(OWNER_ID_KEY, ownerEndId)
                .args(ALBUM_ID_KEY, albumId)
                .args(COUNT_KEY, photoCount),
            VkPhotoResponse::class.java
        ).response?.items ?: emptyList()
    }
}
