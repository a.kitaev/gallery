package ru.itmo.gallery.data.network.response

import com.google.gson.annotations.SerializedName
import ru.itmo.gallery.domain.model.Photo

data class VkPhotosResponse(
    @SerializedName("count")
    val count: Int = 0,
    @SerializedName("items")
    val items: List<Photo> = emptyList()
)
