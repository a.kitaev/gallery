package ru.itmo.gallery.data.utils

import com.ironz.binaryprefs.Preferences
import com.ironz.binaryprefs.PreferencesEditor

inline fun Preferences.modify(commit: Boolean = false, action: PreferencesEditor.() -> Unit) {
    with(edit()) {
        action.invoke(this)
        if (commit) {
            commit()
        } else {
            apply()
        }
    }
}
