package ru.itmo.gallery.data.repository

import androidx.fragment.app.FragmentActivity
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKScope
import ru.itmo.gallery.data.storage.PersistenceStorage
import ru.itmo.gallery.domain.repository.VkAuthRepository

class VkAuthRepositoryImpl(
    private val persistenceStorage: PersistenceStorage
) : VkAuthRepository {

    companion object {

        private val vkScopes = listOf(VKScope.PHOTOS)
    }

    override fun login(activity: FragmentActivity) {
        VK.login(activity, vkScopes)
    }

    override fun saveToken(accessToken: VKAccessToken) {
        accessToken.save(persistenceStorage.accessTokenStorage)
    }

    override fun removeToken() {
        VKAccessToken.remove(persistenceStorage.accessTokenStorage)
    }

    override fun isUserAuthorize(): Boolean {
        val accessToken = VKAccessToken.restore(persistenceStorage.accessTokenStorage)
        return accessToken?.isValid ?: false
    }
}
