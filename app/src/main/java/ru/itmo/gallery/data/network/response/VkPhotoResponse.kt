package ru.itmo.gallery.data.network.response

import com.google.gson.annotations.SerializedName

data class VkPhotoResponse(
    @SerializedName("response")
    val response: VkPhotosResponse? = null
)
