package ru.itmo.gallery.data.repository

import com.google.gson.Gson
import com.vk.api.sdk.VK
import com.vk.api.sdk.VKApiManager
import com.vk.api.sdk.VKMethodCall
import com.vk.api.sdk.internal.ApiCommand

abstract class BaseVkApiRepository {

    protected fun <T> makeResponse(
        builder: VKMethodCall.Builder,
        classT: Class<T>
    ): T = VK.executeSync(
        buildRequest(builder, classT)
    )

    private fun <T> buildRequest(
        builder: VKMethodCall.Builder,
        classT: Class<T>
    ): ApiCommand<T> = object : ApiCommand<T>() {

        override fun onExecute(manager: VKApiManager): T {
            val call = builder
                .version(manager.config.version)
                .build()

            return manager.execute(call) { response ->
                Gson().fromJson(response, classT)
            }
        }
    }
}
