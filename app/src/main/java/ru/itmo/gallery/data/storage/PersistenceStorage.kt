package ru.itmo.gallery.data.storage

import android.content.Context
import com.ironz.binaryprefs.BinaryPreferencesBuilder
import com.ironz.binaryprefs.Preferences
import com.vk.api.sdk.VKKeyValueStorage
import ru.itmo.gallery.data.utils.modify

class PersistenceStorage(context: Context) {

    private val preferences: Preferences =
        BinaryPreferencesBuilder(context)
            .build()

    val accessTokenStorage = object : VKKeyValueStorage {

        override fun get(key: String): String? =
            preferences.getString(key, null)

        override fun put(key: String, value: String) {
            preferences.modify { putString(key, value) }
        }

        override fun remove(key: String) {
            preferences.modify { putString(key, null) }
        }
    }
}
